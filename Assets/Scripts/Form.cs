using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Form : MonoBehaviour
{

    public static event Action<Player> OnUpdate;

    [SerializeField] private TMP_InputField _nickField;
    [SerializeField] private TMP_InputField _descriptionField;
    [SerializeField] private TMP_Dropdown _typeDropdown;
    [SerializeField] private TMP_Dropdown _classDropdown;
    [SerializeField] private TextMeshProUGUI _statusbar;
    public struct Player
    {
        public string nick;
        public string description;
        public string type;
        public string playerClass;
    }

    public void UpdateInfo()
    {
        if (!gameObject.activeSelf)
            return;

        if (_nickField.text.Length != 0 && _nickField.text != "")
        {
            Player player = new Player();

            player.nick = _nickField.text;
            player.description = _descriptionField.text;
            player.type = _typeDropdown.options[_typeDropdown.value].text;
            player.playerClass = _classDropdown.options[_classDropdown.value].text;


            OnUpdate?.Invoke(player);

            _statusbar.text = "";
            gameObject.SetActive(false);

            
        }
        else
        {
            _statusbar.text = "Nickname field is empty!";
            return;
        }
    }
}

