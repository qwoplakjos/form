using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    [SerializeField] private Form _form;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            _form.gameObject.SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            _form.UpdateInfo();
        }
    }
}