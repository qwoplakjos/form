using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class ChangeInfo : MonoBehaviour
{

    [SerializeField] private TextMeshProUGUI _nickText;
    [SerializeField] private TextMeshProUGUI _descriptionText;
    [SerializeField] private TextMeshProUGUI _typeText;
    [SerializeField] private TextMeshProUGUI _classText;

    private void OnEnable()
    {
        Form.OnUpdate += UpdateText;
    }

    private void OnDisable()
    {
        Form.OnUpdate -= UpdateText;
    }

    private void UpdateText(Form.Player playerInfo)
    {
        _nickText.text = playerInfo.nick;
        _descriptionText.text = playerInfo.description;
        _typeText.text = playerInfo.type;
        _classText.text = playerInfo.playerClass;
    }

}
